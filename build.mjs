import { dtsPlugin } from 'esbuild-plugin-d.ts'
import { build } from 'esbuild'
import glob from 'glob-promise'

const entryPoints = await glob('public/ts/**/*.ts')

const javascript = build({
  entryPoints,
  outdir: 'public/js',
  outExtension: { '.js': '.ts', },
  plugins: [ dtsPlugin() ],
  sourcemap: true,
})

const minified = build({
  entryPoints,
  minify: true,
  outdir: 'public/mjs',
  outExtension: { '.js': '.ts', },
  plugins: [ dtsPlugin() ],
  sourcemap: true,
})

await minified, javascript