import instance from './wasm_module.ts'
import { encode as utf8_encode, decode as utf8_decode } from '../../public/deno/ops.ts'

let module: Awaited<typeof instance>
let memory: typeof module.memory
let add_sp: typeof module.__wbindgen_add_to_stack_pointer
let malloc: typeof module.__wbindgen_malloc
let _parse: typeof module.parse
let _parse_frag: typeof module.parse_frag
let free: typeof module.__wbindgen_free

export const init = async () => {
  module = await instance
  memory = module.memory
  add_sp = module.__wbindgen_add_to_stack_pointer
  malloc = module.__wbindgen_malloc
  _parse = module.parse
  _parse_frag = module.parse_frag
  free = module.__wbindgen_free
}

export const parse = (html: string) => {
  try {
    const ret_ptr = add_sp(-8)

    const input_buf = utf8_encode(html)
    const input_len = input_buf.byteLength
    const input_ptr = malloc(input_len)
    new Uint8Array(memory.buffer).set(input_buf, input_ptr)

    _parse(ret_ptr, input_ptr, input_len)

    const [output_ptr, output_len] = new Uint32Array(
      memory.buffer.slice(ret_ptr, ret_ptr + 8),
    )
    try {
      return JSON.parse(
        utf8_decode(memory.buffer.slice(output_ptr, output_ptr + output_len)),
      )
    } finally {
      free(output_ptr, output_len)
    }
  } finally {
    add_sp(8)
  }
}

export const parse_frag = (html: string) => {
  try {
    const ret_ptr = add_sp(-8)

    const input_buf = utf8_encode(html)
    const input_len = input_buf.byteLength
    const input_ptr = malloc(input_len)
    new Uint8Array(memory.buffer).set(input_buf, input_ptr)

    _parse_frag(ret_ptr, input_ptr, input_len)

    const [output_ptr, output_len] = new Uint32Array(
      memory.buffer.slice(ret_ptr, ret_ptr + 8),
    )
    try {
      return JSON.parse(
        utf8_decode(memory.buffer.slice(output_ptr, output_ptr + output_len)),
      )
    } finally {
      free(output_ptr, output_len)
    }
  } finally {
    add_sp(8)
  }
}

await init()
console.log(module!.memory.buffer.byteLength)
console.dir(parse_frag('Hewwo world1234<a b=c>♈♉♊♋♌♍♎♏♐♑♒♓"\'</a>bar'), { depth: 9000 })
console.log(module!.memory.buffer.byteLength)

const test = new Uint8Array(memory!.buffer.slice(1048560, 1048560 + 200))
console.log(test)
