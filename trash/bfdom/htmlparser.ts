import { Parser as html2parser } from 'https://esm.sh/v106/htmlparser2/es2022/lib/esm/Parser.js'

const html_str = `
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hello from Deno</title>
  </head>
  <body>
    <h1>Hello from Deno</h1>
    <form>
      <input name="user">
      <button>
        Submit
      </button>
    </form>
  </body>
</html>
`

const path: string[] = []
const parser = new html2parser({
    onprocessinginstruction(name: string, data: string) {
      console.log('PI', name, 'f', data)
    },
    onopentag(name: string, attributes: Record<string, string>) {
      //console.log('opentag', name, attributes)
      path.push(name)
      console.log(path)
    },
    ontext(text: string) {
      console.log('text', text.trim())
    },
    onclosetag(tagname: string) {
      console.log('closetag', tagname)
      path.pop()
      console.log(path)
    },
  })
  parser.write(html_str)
  parser.end()