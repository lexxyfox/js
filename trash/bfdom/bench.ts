import { DOMParser as DenoDOM } from 'https://deno.land/x/deno_dom/deno-dom-wasm.ts'
import { DOMParser as LinkeDOM } from 'https://esm.sh/linkedom'
import { DOMParser as LinkeDOMESM } from 'https://esm.sh/linkedom/esm'

import { parseFromString as linkedom_parse_from_string } from 'https://esm.sh/linkedom/esm/shared/parse-from-string.js'
import { HTMLDocument as linkedom_html_document } from 'https://esm.sh/linkedom/esm/html/document.js'
import { CUSTOM_ELEMENTS } from 'https://esm.sh/linkedom/esm/shared/symbols.js'

import { parseDocument as htmlparser2_parse_document } from 'https://esm.sh/v106/htmlparser2/es2022/htmlparser2.js'
/////// spam @deno-types="https://unpkg.com/htmlparser2/lib/esm/Parser.d.ts"
import { Parser as html2parser } from 'https://esm.sh/v106/htmlparser2/es2022/lib/esm/Parser.js'

import { DomHandler } from 'https://esm.sh/v106/domhandler/es2022/domhandler.js'

const deno_dom = new DenoDOM()
const linkedom = new LinkeDOM()
const linkedomesm = new LinkeDOMESM()

import * as BFDOM from './main.ts'

const html_str = `
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hello from Deno</title>
  </head>
  <body>
    <h1>Hello from Deno</h1>
    <form>
      <input name="user">
      <button>
        Submit
      </button>
    </form>
  </body>
</html>
`

Deno.bench('deno-dom as instructed', () => {
  deno_dom.parseFromString(html_str, 'text/html')
})

Deno.bench('deno-dom wasm layer', () => {
  BFDOM.parse(html_str)
})

Deno.bench('linkeDOM as instructed', () => {
  linkedom.parseFromString(html_str, 'text/html')
})

Deno.bench('linkeDOM ESM layer -1', () => {
  linkedomesm.parseFromString(html_str, 'text/html')
})

/*
Deno.bench('linkeDOM ESM layer -2', () => {
  const html_document = new linkedom_html_document()
  ;(html_document as any)[CUSTOM_ELEMENTS] = { active: false, registry: {} }
  linkedom_parse_from_string(html_document, true, html_str)
})
*/

Deno.bench('HTML2Parser (linkeDOM layer -3)', () => {
  //const parser = new HTMLParser2({})
  //parser.write(html_str)
  //parser.end()
  htmlparser2_parse_document(html_str)
})

Deno.bench('HTML2Parser layer -1', () => {
  const parser = new html2parser({})
  parser.write(html_str)
  parser.end()
})

/*
const parser = new html2parser({
  onprocessinginstruction(name, data) {
    console.log('PI', name, data)
  },
  onopentag(name, attributes) {
    console.log('opentag', name, attributes)
  },
  ontext(text) {
    console.log('text', text)
  },
  onclosetag(tagname) {
    console.log('closetag', tagname)
  },
})
parser.write(html_str)
parser.end()
*/

const handler = new DomHandler(undefined, {})
const parser = new html2parser(handler, {})
parser.write(html_str)
parser.end()
