const ops =
  typeof Deno === 'object'
    ? // deno-lint-ignore no-explicit-any
      ((Deno as any)[(Deno as any).internal].core.ops as any)
    : {}

const char_code_at = (c: string) => c.charCodeAt(0)

/**
 * Decodes a string of data which has been encoded using base-64 encoding.
 * Conforms to [WHATWG's forgiving base64](https://infra.spec.whatwg.org/#forgiving-base64).
 *
 * [deno:ext/web/05_base64.js#atob](https://github.com/denoland/deno/blob/main/ext/web/05_base64.js#L21-L43)
 *
 * [deno:ext/web/lib.rs#op_base64_decode](https://github.com/denoland/deno/blob/-/ext/web/lib.rs#L130-L136)
 *
 * [deno:ext/web/lib.rs#forgiving_base64_decode_inplace](https://github.com/denoland/deno/blob/main/ext/web/lib.rs#L145-L154)
 *
 * [base64_simd::forgiving_decode_inplace](https://docs.rs/base64-simd/latest/base64_simd/fn.forgiving_decode_inplace.html)
 *
 * [base64_simd:forgiving.rs#forgiving_decode_inplace](https://docs.rs/base64-simd/latest/src/base64_simd/forgiving.rs.html#12-24)
 *
 * [base64_simd:lib.rs#decode_inplace](https://docs.rs/base64-simd/latest/src/base64_simd/lib.rs.html#279-294)
 */
export const base64_decode: (input: string) => Uint8Array =
  ops.op_base64_decode ?? (input => Uint8Array.from(atob(input), char_code_at))

/**
 * Creates a base-64 ASCII encoded string from the input BufferSource.
 * Conforms to [WHATWG's forgiving base64](https://infra.spec.whatwg.org/#forgiving-base64).
 *
 * [deno:ext/web/05_base64.js#btoa](https://github.com/denoland/deno/blob/-/ext/web/05_base64.js#L45-L67)
 *
 * [deno:ext/web/lib.rs#op_base64_encode](https://github.com/denoland/deno/blob/-/ext/web/lib.rs#L157-L160)
 *
 * [deno:ext/web/lib.rs#forgiving_base64_encode](https://github.com/denoland/deno/blob/-/ext/web/lib.rs#L167-L171)
 *
 * [base64_simd::STANDARD.encode_to_string()](https://docs.rs/base64-simd/latest/base64_simd/struct.Base64.html#method.encode_to_string)
 *
 * [base64_simd:lib.rs#encode_to_string](https://docs.rs/base64-simd/latest/src/base64_simd/lib.rs.html#327-334)
 *
 * [base64_simd:lib.rs#encode_type](https://docs.rs/base64-simd/latest/src/base64_simd/lib.rs.html#296-301)
 */
export const base64_encode: (input: BufferSource) => string =
  ops.op_base64_encode ??
  (input =>
    // @ts-ignore:
    btoa(String.fromCharCode.apply(null, Array.from(new Uint8Array(input)))))

/**
 * Sets the value of `buf` to a high-precision 64-bit value representing the number of nanoseconds elapsed since the [time origin](https://www.w3.org/TR/hr-time-2/#dfn-time-origin).
 * `buf` must be a buffer of at least 8 bytes.
 * Using a buffer of fewer than 8 bytes will result in undefined behaviour.
 * Buffer space beyond the 8th byte will never be affected.
 *
 * The first four bytes represent the number of whole seconds as an unsigned integer,
 * and the latter four bytes represent the remaining number of nanoseconds as an unsigned integer.
 *
 * To offer protection against timing attacks and fingerprinting,
 * the resulting value may be rounded depending on runtime settings.
 * * Deno: [--allow-hrtime](https://deno.land/manual/basics/permissions#permissions-list) / ['hrtime' Permission](https://deno.land/api?s=Deno.HrtimePermissionDescriptor)
 * * Firefox: privacy.reduceTimerPrecision
 *
 * [deno:ext/web/02_timers.js#opNow](https://github.com/denoland/deno/blob/main/ext/web/02_timers.js#L32-L35)
 */
export const now: (buf: BufferSource) => void =
  ops.op_now ??
  (buf => {
    const now = performance.now(),
      view = new Uint32Array(ArrayBuffer.isView(buf) ? buf.buffer : buf)
    view[0] = Math.floor(now / 1000)
    view[1] = (now % 1000) * 1000000
  })
