import { assertEquals } from 'https://deno.land/std@0.176.0/testing/asserts.ts'
import * as ops from './builtin.ts'

Deno.test('decode 𠮷 from Uint8Array', () => {
  assertEquals(ops.decode(new Uint8Array([240, 160, 174, 183])), '𠮷')
})

Deno.test('decode 𠮷 from Int8Array', () => {
  assertEquals(ops.decode(new Int8Array([-16, -96, -82, -73])), '𠮷')
})

Deno.test('decode 𠮷 from Uint16Array', () => {
  assertEquals(ops.decode(new Uint16Array([41200, 47022])), '𠮷')
})

Deno.test('decode 𠮷 from Int16Array', () => {
  assertEquals(ops.decode(new Int16Array([-24336, -18514])), '𠮷')
})

Deno.test('decode 𠮷 from Int32Array', () => {
  assertEquals(ops.decode(new Int32Array([-1213292304])), '𠮷')
})

Deno.test('decode Привет, мир from Uint8Array', () => {
  assertEquals(
    ops.decode(
      new Uint8Array([
        208, 159, 209, 128, 208, 184, 208, 178, 208, 181, 209, 130, 44, 32, 208,
        188, 208, 184, 209, 128, 33,
      ]),
    ),
    'Привет, мир!',
  )
})

Deno.test('encode sample paragraph', () => {
  assertEquals(
    ops.encode('This is a sample paragraph.'),
    new Uint8Array([
      84, 104, 105, 115, 32, 105, 115, 32, 97, 32, 115, 97, 109, 112, 108, 101,
      32, 112, 97, 114, 97, 103, 114, 97, 112, 104, 46,
    ]),
  )
})