const ops =
  typeof Deno === 'object'
    ? // deno-lint-ignore no-explicit-any
      ((Deno as any)[(Deno as any).internal].core.ops as any)
    : {}

/*
export type RecordType =
  | 'A' | 'AAAA' | 'ANAME' | 'ANY' | 'AXFR' | 'CAA' | 'CDS' | 'CDNSKEY' | 'CNAME' | 'CSYNC' | 'DNSKEY' | 'DS' | 'HINFO' | 'HTTPS' | 'IXFR' | 'KEY' | 'MX' | 'NAPTR' | 'NS' | 'NSEC' | 'NSEC3' | 'NSEC3PARAM' | 'NULL' | 'OPENPGPKEY' | 'OPT' | 'PTR' | 'RRSIG' | 'SIG' | 'SOA' | 'SRV' | 'SSHFP' | 'SVCB' | 'TLSA' | 'TSIG' | 'TXT' | 'ZERO'
  | { Unknown: number } // https://docs.rs/trust-dns-proto/0.22.0/src/trust_dns_proto/rr/record_data.rs.html#682-688
*/

export type RecordType =
  | 'A'
  | 'AAAA'
  | 'ANAME'
  | 'CAA'
  | 'CNAME'
  | 'MX'
  | 'NAPTR'
  | 'NS'
  | 'PTR'
  | 'SOA'
  | 'SRV'
  | 'TXT'
  | { Unknown: number }

export type CAARecord = {
  critical: boolean
  tag: string
  value: string
}

export type MXRecord = {
  preference: number
  exchange: string
}

export type NAPTRRecord = {
  order: number
  preference: number
  flags: string
  services: string
  regexp: string
  replacement: string
}

export type SOARecord = {
  mname: string
  rname: string
  serial: number
  refresh: number
  retry: number
  expire: number
  minimum: number
}

export type SRVRecord = {
  priority: number
  weight: number
  port: number
  target: string
}

/**
 * [deno:ext/net/ops.rs#NameServer](https://github.com/denoland/deno/blob/-/ext/net/ops.rs#L434-L440)
 */
export type NameServer = {
  ipAddr: string
  /** @default 53 */
  port: number
}

/**
 * [deno:ext/net/ops.rs#ResolveDnsOption](https://github.com/denoland/deno/blob/-/ext/net/ops.rs#L424-L428)
 */
export type ResolveDnsOptions = {
  nameServer?: NameServer
}

/**
 * Performs DNS resolution against the given query, returning resolved records.
 *
 * [deno:ext/net/ops.rs#op_dns_resolve](https://github.com/denoland/deno/blob/-/ext/net/ops.rs#L442-L506)
 *
 * [deno:ext/net/ops.rs#rdata_to_return_record](https://github.com/denoland/deno/blob/-/ext/net/ops.rs#L530-L624)
 *
 * [use trust_dns_resolver](https://docs.rs/trust-dns-resolver/latest/trust_dns_resolver/#using-the-tokioasync-resolver)
 */
export const dns_resolve: ((args: {
  query: string
  recordType: 'A' | 'AAAA' | 'ANAME' | 'CNAME' | 'NS' | 'PTR'
  options?: ResolveDnsOptions
}) => Promise<string[]>) &
  ((args: {
    query: string
    recordType: 'MX'
    options?: ResolveDnsOptions
  }) => Promise<MXRecord[]>) &
  ((args: {
    query: string
    recordType: 'CAA'
    options?: ResolveDnsOptions
  }) => Promise<CAARecord[]>) &
  ((args: {
    query: string
    recordType: 'NAPTR'
    options?: ResolveDnsOptions
  }) => Promise<NAPTRRecord[]>) &
  ((args: {
    query: string
    recordType: 'SOA'
    options?: ResolveDnsOptions
  }) => Promise<SOARecord[]>) &
  ((args: {
    query: string
    recordType: 'SRV'
    options?: ResolveDnsOptions
  }) => Promise<SRVRecord[]>) &
  ((args: {
    query: string
    recordType: 'TXT'
    options?: ResolveDnsOptions
  }) => Promise<string[][]>) =
  ops.op_dns_resolve ??
  (_ => {
    throw 'NotConnected: No connections available'
  })
