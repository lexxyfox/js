/** TS types that can be serialized and matched as `None` in Rust */
export type NoneLike = null | undefined | number | boolean