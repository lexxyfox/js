export const compress_buffer = async (
  input: Uint8Array,
  format = 'deflate-raw',
) => {
  const cs = new CompressionStream(format)
  const writer = cs.writable.getWriter()
  writer.write(input)
  writer.close()
  const chunks = []
  const reader = cs.readable.getReader()
  let size = 0,
    offset = 0
  while (true) {
    const { value, done } = await reader.read()
    if (done) break
    chunks.push(value)
    size += value.byteLength
  }
  const concatenated = new Uint8Array(size)
  for (const array of chunks) {
    concatenated.set(array, offset)
    offset += array.byteLength
  }
  return concatenated
}

export const decompress_buffer = async (
  input: Uint8Array,
  format = 'deflate-raw',
) => {
  const ds = new DecompressionStream(format)
  const writer = ds.writable.getWriter()
  writer.write(input)
  writer.close()
  const chunks = []
  const reader = ds.readable.getReader()
  let size = 0,
    offset = 0
  while (true) {
    const { value, done } = await reader.read()
    if (done) break
    chunks.push(value)
    size += value.byteLength
  }
  const concatenated = new Uint8Array(size)
  for (const array of chunks) {
    concatenated.set(array, offset)
    offset += array.byteLength
  }
  return concatenated
}
