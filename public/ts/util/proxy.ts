// @deno-types="https://unpkg.com/p-limit@4.0.0/index.d.ts"
import pLimit from 'https://esm.sh/v106/p-limit@4.0.0/es2022/p-limit.js'
import { resolve } from 'node:path/posix'
import { resourceLimits } from 'node:worker_threads'

// https://observablehq.com/@ehouais/multiple-promises-as-an-async-generator

/**
 * Takes a generator of proxy urls and only yields working proxies.
 *
 * Helps generate a list of working proxies that can be cached for faster requests.
 *
 * @param options.test_urls a list of urls to test. a url is chosen at random to prevent overloading a single server. make sure you trust all of the given urls.
 * @param options.client options passed to the Deno.createHttpClient constructor.
 * @param options.fetch function used to fetch()
 * @param options.timeout number of milliseconds to wait for a response
 * @param options.concurrency number of proxies to try simultaneously
 */
export const test_proxies = async function* (
  urls: AsyncGenerator<string, void, void>,
  options: {
    concurrency?: number
    test_urls?: string[]
    client?: {
      caCerts?: string[]
      certChain?: string
      privateKey?: string
    }
    fetch?: typeof fetch
    timeout?: number
  } = {},
) {
  const test_urls = options.test_urls ?? [
    'http://captive.apple.com',
    'http://captiveportal.kuketz.de',
    'http://clients3.google.com/generate_204',
    'http://detectportal.firefox.com',
    'http://httpstat.us/204',
    'http://www.gstatic.com/generate_204',
    'https://elementary.io/generate_204',
  ]
  const client_options = options.client ?? {}
  const fetcher = options.fetch ?? fetch
  const timeout = options.timeout ?? 1000
  const concurrency = options.concurrency ?? 32

  const limit = pLimit(concurrency)
  const map = new Map<string, Promise<[string, boolean]>>()

  let next_url = urls.next()

  ;(async () => {
    while (true) {
      const { done, value: proxy_url } = await next_url
      if (done) break
      const test_url = test_urls[Math.floor(Math.random() * test_urls.length)]
      map.set(
        proxy_url,
        limit(async () => {
          //console.log('trying', proxy_url)
          try {
            await fetcher(test_url, {
              signal: AbortSignal.timeout(timeout),
              client: Deno.createHttpClient({
                ...client_options,
                proxy: {
                  url: proxy_url,
                },
              }),
            })
            //console.log(result)
            return [proxy_url, true]
          } catch {
            return [proxy_url, false]
          }
        }),
      )
      next_url = urls.next()
    }
  })()

  while (map.size > 0 || !(await next_url).done) {
    //console.log('awaiting', map.size)
    const [key, result] = await Promise.race(map.values())
    //console.log('yielding!', map.size)
    if (result) yield key
    map.delete(key)
  }
}
