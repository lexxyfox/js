// https://doc.rust-lang.org/std/iter/trait.Iterator.html

export const take = async function* <T>(
  iterable: AsyncIterator<T>,
  n: number,
) {
  let count = n
  while (count-- > 0) {
    const s = await iterable.next()
    if (!s.done) yield s.value
    else return
  }
}

export const collect = async <T>(iterable: AsyncIterable<T>) => {
  const arr = []
  for await (const i of iterable) arr.push(i)
  return arr
}
