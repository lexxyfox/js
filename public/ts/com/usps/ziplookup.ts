export type AddressResult = {
  addressLine1: string
  city: string
  state: string
  zip5: string
  zip4: string
  carrierRoute: string
  countyName: string
  deliveryPoint?: string
  checkDigit?: string
  cmar: string
  elot?: string
  elotIndicator?: string
  recordType?: string
  dpvConfirmation?: string
  defaultFlag?: string
  defaultInd?: string
}

export type ZipByAddressRequest =
  | {
      address1?: string
      address2?: string
      city: string
      companyName?: string
      state: string
      urbanCode?: string
      zip?: string | number
    }
  | {
      address1?: string
      address2?: string
      city?: string
      companyName?: string
      state?: string
      urbanCode?: string
      zip: string | number
    }

export const zipByAddress = async (data: ZipByAddressRequest) => {
  const response = await (
    await fetch('https://tools.usps.com/tools/app/ziplookup/zipByAddress', {
      // deno-lint-ignore no-explicit-any
      body: new URLSearchParams(data as any),
      method: 'POST',
    })
  ).json()
  if (response.resultStatus !== 'SUCCESS') throw response.resultStatus
  return response.addressList as AddressResult[]
}