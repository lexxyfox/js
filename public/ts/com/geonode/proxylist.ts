export type AnonymityLevel = 'elite' | 'anonymous' | 'transparent'

export type Proxy = {
  _id: string
  ip: string
  anonymityLevel: AnonymityLevel
  asn: string
  city: string
  country: string
  created_at: Date
  google: boolean
  isp: string
  /** Indicates the last `Date` the proxy's status was last checked. */
  lastChecked: Date
  latency: number
  org: string
  port: number
  protocols: ('socks4' | 'socks5' | 'http' | 'https')[]
  //region: null
  responseTime: number
  speed: number
  updated_at: Date
  //workingPercent: null
  /** Indicates the percent of the time this proxy has been up. Equivalent to (`upTimeSuccessCount` / `upTimeTryCount`) * 100 */
  upTime: number
  upTimeSuccessCount: number
  upTimeTryCount: number
}

export type SearchOptions = {
  /** Only proxies with an `anonymityLevel` that match exactly will be returned. */
  anonymityLevel?: AnonymityLevel
  /** Only proxies with a `country` that match exactly will be returned. */
  country?: string
  /** Only proxies that have been checked within the last given number of minutes will be returned. Not very useful as the list only seems to be checked at most once every 60 minutes. */
  filterLastChecked?: 60 | 50 | 40 | 30 | 20 | 10 | 9 | 8 | 7 | 6 | 5
  /** Only proxies with an `org` that match exactly will be returned. */
  org?: string
  /** Only proxies with a port number matching the given port are returned. */
  port?: number
  sort_type?: 'asc' | 'desc'
  sort_by?:
    | 'anonymityLevel'
    | 'city'
    | 'country'
    | 'google'
    | 'isp'
    | 'lastChecked'
    | 'latency'
    | 'org'
    | 'responseTime'
    | 'speed'
    | 'upTime'
  speed?: 'fast' | 'medium' | 'slow'
  /** Only proxies with an uptime of *at least* this number (as a percent) are returned. */
  upTime?: 90 | 80 | 70 | 60 | 50 | 40 | 30 | 20 | 10
}

type Fetch = typeof fetch

export const list = async function* (
  query: SearchOptions = {},
  fetcher: Fetch = fetch,
): AsyncGenerator<Proxy, void, void> {
  // @ts-ignore:
  ;(query = { ...query }).page = 1
  // @ts-ignore:
  query.limit ??= 500
  if ('org' in query)
    // @ts-ignore:
    (query.filterByOrg = query.org), delete query.org
  if ('port' in query)
    // @ts-ignore:
    (query.filterPort = query.port), delete query.port
  if ('upTime' in query)
    // @ts-ignore:
    (query.filterUpTime = query.upTime), delete query.upTime
  while (true) {
    const response = await (
      await fetcher(
        'https://proxylist.geonode.com/api/proxy-list?' +
          // deno-lint-ignore no-explicit-any
          new URLSearchParams(query as any),
      )
    ).json()
    //console.log('got page from geonode')
    if (response.data === null) throw response.message
    //console.log('THROW', response.data)
    if (response.data.length < 1) return
    for (const proxy of response.data) {
      proxy.created_at = new Date(proxy.created_at)
      proxy.lastChecked = new Date(proxy.lastChecked * 1000)
      proxy.updated_at = new Date(proxy.updated_at)
      proxy.port = parseInt(proxy.port)
      delete proxy.region
      delete proxy.workingPercent
      yield proxy
    }
    // @ts-ignore:
    query.page++
  }
}

/** As the return type suggests, returns some quick information about GeoNode's proxy list service. Interestingly howerver, these numbers don't seem to accurately reflect the results obtained by the `list` or `countries` functions. */
export const summary = async (fetcher: Fetch = fetch) => {
  const summary = (
    await (
      await fetcher('https://proxylist.geonode.com/api/proxy-summary')
    ).json()
  ).summary
  summary.lastUpdated = new Date(summary.lastUpdated)
  return summary as {
    lastUpdated: Date
    proxiesOnline: number
    countriesOnline: number
  }
}

/**
 * @returns A list of country codes available for use in the `list` function's `country` search parameter.
 */
export const countries = async (fetcher: Fetch = fetch) =>
  (await (await fetcher('https://proxylist.geonode.com/api/countries')).json())
    .data as string[]

/*
const limit = 5
let i = 1
for await (const proxy of list({ sort_by: 'google' })) {
  if (i++ > limit) break
  console.log(proxy)
}
*/

// https://github.com/denoland/deno/discussions/17606

/**
 * Converts a generator of GeoNode Proxies into proxy URLs compatible with Deno's `createHttpClient`.
 */
export const as_urls = async function* (
  proxies?: AsyncGenerator<Proxy, void, void>,
) {
  proxies ??= list()
  for await (const proxy of proxies)
    for (const proto of proxy.protocols) {
      if (proto === 'socks4') continue
      yield `${proto}://${proxy.ip}:${proxy.port}`
    }
}